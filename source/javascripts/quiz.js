window.onload = function() {
  new Vue({
    el: '#quiz',
    data: {
      quiz: quiz,
      answered: Array(quiz.questions.length)
    },
    methods: {
      check: function(question, q) {
        var answer = this.answered[q];
        if (typeof(answer) === 'undefined') return null;
        if (answer === question.correct) return 'correct';
        return 'wrong'
      }
    }
  });
}
